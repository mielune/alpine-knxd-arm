FROM hypriot/rpi-alpine

## Choose between branches
ARG BRANCH=v0.14

RUN apk add --no-cache build-base gcc abuild binutils binutils-doc gcc-doc git libev-dev automake autoconf libtool argp-standalone linux-headers libusb-dev cmake cmake-doc dev86 \
    && mkdir -p /usr/local/src && cd /usr/local/src \
    && git clone https://github.com/knxd/knxd.git --single-branch --branch $BRANCH \
    && cd knxd && ./bootstrap.sh \
    && ./configure --disable-systemd --enable-eibnetip --enable-eibnetserver --enable-eibnetiptunnel \
    && mkdir -p src/include/sys && ln -s /usr/lib/bcc/include/sys/cdefs.h src/include/sys \
    && make && make install && cd .. && rm -rf knxd \
    && addgroup -S knxd \
    && adduser -D -S -s /sbin/nologin -G knxd knxd \
    && apk del --no-cache build-base abuild binutils binutils-doc gcc-doc git automake autoconf libtool argp-standalone cmake cmake-doc dev86

COPY entrypoint.sh /
RUN chmod a+x /entrypoint.sh

EXPOSE 3672 6720
#ENV ARGS "--daemon -D -T -S --listen-tcp=6720 -S --listen-tcp=6720 -t 5   ipt:192.168.144.145"
#ENV ARGS "--daemon=/logs/knxd.log -e 0.0.1 --GroupCache -D -T --listen-tcp=6720 --layer2=ipt:192.168.144.145"
ENV ARGS "-d -e 0.0.1 -E 0.0.2:8 -c -D -T -S -i 6720 -b ipt:192.168.144.145"

CMD "/entrypoint.sh"
