#!/bin/sh

echo "Starting knxd service."
echo "ARGS: $ARGS"
echo "Press <ctrl>-c to abort"
su -s /bin/sh -c "knxd $ARGS" knxd

# Workaround because knxd always forks to background
while [ true ] ; do
    sleep 5
done
